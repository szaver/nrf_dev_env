# Nordic nRF5 SDK Development Environment

This project contains an Ansible provisioning script and a Vagrantfile to provision a development environment for the Nordic nRF5 SDK.  Whereas most embedded development setups seem to prefer Eclipse and Windows, I wanted something much simpler with only light abstraction over the bare tools underneath.  This setup builds with GNU Make, and flashes with Nordic's nrfjprog software.  The makefile can build and run in one step with the `make flash` command, but the generated hex files are right there in the `_build` directory so you can run `nrfjprog` directly.

Instead of creating a custom Vagrant box, I felt it preferrable to share my Ansible provisioning script.  This way the same script can be used to provision other machines.  As long as it is running Ubuntu 18.04 LTS it should work.  Probably other versions would work as well.  PR's welcome!

## Features

* Nordic nRF5 SDK
* arm-none-eabi GCC toolchain for cross compilation
* Loosened SSH crypto settings for compatibility with JetBrains CLion remote mode.  (Note: only connection has been tested.  I use VSCode.)
* Configuration files for VSCode so intellisense
* Lots of useful tools installed for you like neovim, ripgrep, wget, git, and tree (TODO: some of these are not installed yet)

## How Everything Fits Together

* VirtualBox is a virtual machine hypervisor.  It allows you to install other operating systems on your computer.  The operating systems think and act like they are running on bare metal, but in fact they are only accessing a small slice of the total resources available to the host machine.

* Vagrant is a virtual machine manager.  It is a nice CLI interface that makes it easy to create and destroy virtual machines.  Vagrant also has a library of pre-configured virtual machines called "boxes".  Some are base machines like 'Ubuntu 18.04 LTS'.  Others, like Laravel Homestead, contain a suite of development tools.  We're using the vanilla 'Ubuntu 18.04', in case you're wondering.  Finally Vagrant includes support for a number of provisioners, including Ansible which is what we're using.  A provisioner is some program that runs to bring the Vagrant box to its final configuration.  It could be as simple as a shell script, but Ansible provides some neat benefits which we'll discuss next.

* I like to think of Ansible as shell scripts, but more idempotent.  Idempotency means that an operation can run multiple times with the same result.  If you have ever written Bash scripts you will know that unless you're careful the script will not like to be run twice.  On the first run you might create a file, on the second run an error is thrown because the file already exists and would be overwritten.  In Ansible you describe the desired end-state and it will skip actions which it detects have already been performed.  Ansible scripts are actually written with YAML, a language traditionally reserved for configuration.

* All your projects in this development environment should be a subdirectory of `nrf_dev_env/projects/`.  Contents of this directory are ignored by git (see ".gitignore") so you can `git pull` new updates to the development environment without disturbing your work in progress.

* The Nordic nRF5 SDK and command line tools are bundled in this repository via Git LFS to ensure compatibility and so you don't have to go out and hunt down a download.

## Getting Started

This guide has been written for use on MacOS, to provision a Ubuntu 18.04 machine.  Instructions related to Vagrant assume that you are using Vagrant's VirtualBox backend.  Finally, in some cases I am assuming that you are using Nordic's official PCA10056 nRF52840 Development Kit version 2.0.1.  You may need to make slight modifications along the way to program a different board.  I will try to call these out as they come up in the guide.

I would like to give credit to SparkFun's [nRF52840 Advanced Development With the nRF5 SDK](https://learn.sparkfun.com/tutorials/nrf52840-advanced-development-with-the-nrf5-sdk#introduction) guide, where I learned much of the information I am relaying here.  It includes many examples on how to extend the SDK to support custom boards, and links to a dditional resources.

### Prerequisites

#### Homebrew

See https://brew.sh for installation instructions.  Homebrew is a package manager for MacOS.  It is highly recommended, as it automates many parts of the installation, but not required.

#### Vagrant

NOTE: Vagrant is not required if you wish to provision an existing Ubuntu Installation

See https://www.vagrantup.com/intro/getting-started/install for installation instructions.  I personally prefer to install vagrant via [Hombrew](https://brew.sh) [Cask](https://github.com/Homebrew/homebrew-cask) (i.e. `brew cask install vagrant`) but that workflow is not recommended by HashCorp.

Go ahead and run through the [getting started](https://www.vagrantup.com/intro/getting-started/project_setup) section of the Vagrant guide to ensure that everything is working correctly.

#### Ansible

Again, I prefer to install this with Homebrew (`brew install ansible`) but suit yourself.  Official installation instructions for MacOS are here: https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html#installing-ansible-on-macos


Ansible has a [bug](https://github.com/ansible/ansible/issues/32499) on MacOS which causes it crash from time to time.  The fix is easy.  Just add the following line to your "~/.zshrc" file (or "~/.bashrc" if you haven't upgraded to zsh yet):

```sh
export OBJC_DISABLE_INITIALIZE_FORK_SAFETY=YES
```

You will also need to install GNU Tar, as it is required by some Ansible commands.  You can find some discussion of the issue [here](https://stackoverflow.com/questions/54528115/unable-to-extract-tar-file-though-ansible-unarchive-module-in-macos).  In summary, I solve it for myself like this:

1. Install GNU tar with the following command: `brew install gnu-tar`
2. Add this line to my "~/.zshrc": `PATH="/usr/local/opt/gnu-tar/libexec/gnubin:$PATH"`.  This ensures that GNU tar comes first in the PATH and overrides Mac's BSD tar.

### Clone the repository

```sh
$ git clone --depth=1 https://gitlab.com/szaver/nrf_dev_env.git
$ cd nrf_dev_env
```

Unless otherwise noted, all commands after this will assume you're in the "nrf_dev_env" directory.

#### Receiving Updates

Start with this:

```sh
$ git pull
```

Now check the output of that command and look for a list of files changed.  If changes were made to the "Vagrantfile" then you will need to destroy the vm (`vagrant destroy`) and rebuild (follow the instructions under "Provision a new VM with Vagrant").  If the changes are only to "ansible-provision.yml", then you can simply re-provision the vm (`vagrant provision`).  Ansible should be smart enough to handle the migration.

### Provision a new VM with Vagrant

This is the easiest part.  Run `vagrant up` to download and build the machine.  Vagrant will call out to ansible under the hood to complete provisioning on the first run.  Vagrant will also generate a new ssh keypair and store it in `.vagrant/machines/default/virtualbox/private_key`.

Run `vagrant ssh` to log in.  If you need more options than `vagrant ssh` provides, you can use the following `ssh` command as a starting point:

```sh
$ ssh vagrant@192.168.33.10 -i .vagrant/machines/default/virtualbox/private_key
```

#### VirtualBox USB Passthrough

Before you can start programming a device, you will need to tell VirtualBox to connect the USB device to the virtual machine.

Try opening the VirtualBox app now.  You should see a list of virtual machines, including one created by Vagrant that is currently running.  You will need to stop it before you can change the settings.  `vagrant halt` should do the trick there.

You are going to be defining a USB device filter which automatically connects all devices matching a certain pattern to the Virtual Machine.  This will be a lot easier if you have your dev board or programmer already connected.  Done that?  Okay, let's continue.

Now click on the selected virtual machine and then click Settings in the toolbar.  Go to the "Ports" tab and select the "USB" sub-tab.  Check the box labled "Enable USB Controller" and then click the USB plus icon on the right.  A drop-down list of USB devices will appear; select the entry for your dev board or programmer ("SEGGER J-LINK" in my case) If you're not sure which one to select, you could try disconnecting your USB device and remembering that any device shown at that point is *not* your programmer.  Then connect again and see what shows up.

Now select your newly added programmer from the list and click the USB gear icon.  If you only want this exact programmer to be passed through to the device, then the settings can be left as-is.  I prefer to blank out a few fields like serial number and revision so that other devkits will be selected by the same rule.  Click OK to leave the dialog and ensure that the USB Device Filter you just created is enabled (box is checked).  Click OK again to leave the settings page, then run `vagrant up` in your terminal to start the virtual machine again.  You're good to go!

### Provision an existing Ubuntu machine using Ansible

NOTE: This section assumes that you already have a Ubuntu machine setup and accessible via SSH.  If you want Vagrant to do that for you, please see the previous section

Don't want to create a virtual machine with Vagrant?  No problem.  You can still use Ansible to provision the machine of your choosing.  The gist is as follows:

```sh
$ ansible-playbook -i hosts ansible-provision.yml
```

`-i hosts` refers to an inventory file which you will need to create.  Please refer to the [documentation](https://docs.ansible.com/ansible/latest/user_guide/intro_inventory.html) for details.  A simple inventory file might look like this:

```ini
ubuntu@192.168.1.1
```

Ansible is designed to provision whole fleets of servers in parallel, so there are facilities for putting servers into groups and only running tasks on certain servers.  But our setup does not have that requirement, so "ansible-provision.yml" simply specifies `hosts: all` meaning that any host defined in the inventory file will be provisioned.  You can also constrain the hosts on which to run a playbook with the `-l` flag.  For example `ansible-playbook -i hosts -l vm2 ansible-provision.yml` would only run the playbook on a host matching that name (i.e. group name or hostname contains `vm2`).

NOTE: if the installation of Ubuntu which you just provisioned is running in a VM, don't forget to enable USB passthrough.

### Flashing an example

Now that your vm is set up, it's time to look around and flash some code.  A good place to start would be the blinky example.

```sh
$ cd ~/nrf_sdk/examples/peripheral/blinky
```

Here you see the code, and a bunch of folders like "pca10040" and "pca10056".  Those represent different different boards the example can be built for.  I have the NRF52840 DK, also known as the pca10056, so let's go in there.

```sh
$ cd pca10056
```

Now we find ourselves with two more folders.  The "blank" folder is what we want for now.  The "mbr" folder starts writing the program at 0x100 instead of 0x0 in flash to leave space for a bootloader.

```sh
$ cd blank
```

Now we are confronted with a list of toolchains.  Nordic helpfully includes example configurations for a number of different toolchains, but this development environment was set up for GCC, so let's go into that directory.

```sh
$ cd armgcc
```

Finally we can see a Makefile and a linker script.  Very good.  You may like to run `nvim Makefile` to poke around. The makefile commands to know are:

* `make` to build the code
* `make flash` to build the code and flash it to the device
* `make erase` erases the device

Go ahead and run `make flash`.  If all goes well the command should complete successfully and you should see an LED chase on your devboard.  Doesn't work?  Check the troubleshooting section below.

#### Helpful Bash shortcuts
The nRF SDK has some very deeply nested directory structures.  I have taken the liberty of defining some bash aliases to make it easier to navigate around.  They are inserted at the end of "~/.bashrc", so feel free to take a look for yourself.

* `dblinky` Runs `cd $HOME/nrf_sdk/examples/peripheral/blinky` to take you to the blinky example.
* `dble` takes you to the bluetooth blinky example.
* `dbuild` Takes you to the build files, assuming you are in the top directory of an example (i.e. where the source code is located).
* `dcode` Takes you to the top level of an example, assuming you are in the build files directory.

The next section has some examples of these shortcuts in action.

#### Flashing a Bluetooth example

Let's switch to the Bluetooth blinky example using the alias we talked about in the previous section:

```
$ dble
```

The Bluetooth examples have a slightly different directory structure.  Here is an abreviated output of the tree command:

```
.
├── ble_app_blinky.eww
├── hex
├── main.c
├── pca10040
├── pca10040e
└── pca10056
    └── s140
        ├── arm5_no_packs
        ├── armgcc
        │   ├── Makefile
        │   └── ble_app_blinky_gcc_nrf52.ld
        ├── config
        │   └── sdk_config.h
        ├── iar
        └── ses
```

Inside "pca10056" we no longer see two directories labeled "blank" and "mbr".  Instead, we see a single directory called "s140".  As it turns out, for a long time Nordic did not publicly release the code for their Bluetooth stack.  Instead they released binary blobs called [SoftDevices](https://infocenter.nordicsemi.com/index.jsp?topic=%2Fstruct_nrf52%2Fstruct%2Fnrf52_softdevices.html) which were designed to be programmed onto the device alongside the user code.  I believe the s140 SoftDevice is the recommended one for the nRF52840 which is what we're programming today.  Fortunately Nordic has partnered with the ZephyrOS team and the Linux Foundation to release an open-source Bluetooth stack as part of ZephyrOS.  But there are still reasons to use the SDK directly, namely simplicity, better documentation (for now at least), and greater control over power consumption, and that is what we're doing today.

Let's descend into the build directory.  Our `dbuild` alias is smart and will automatically go into the "s140" directory, or the "build" directory, whichever is first.

```sh
$ dbuild
```

Feel free to go ahead and run `make flash` to build and flash the code.  But it won't work yet.  In fact, the blinky example will probably continue running.  That is because the code you just flashed was written to flash starting at address 0x2700.  Space is being left for something else at the beginning of memory, and that something is the s140 SoftDevice.  Before the code will work, you need to run `make flash_softdevice` to overwrite your blinky code and fill the hole at the beginning.

Once the code is flashed, try downloading the nRF Connect app on your phone, and scanning for devices.  You should see a see a device named "Nordic_Blinky".  Go ahead and connect to it, then swipe to the next page to see available services.  Tap on the many down arrow button to subscribe to changes in the state, then try holding down user button one.  The value should display as `0x01`.  Now release the button.  The value should display `0x00`.  Pretty cool!

Moving on to the LED state attribute.  Tap the up arrow to send a value, then select "byte array" and write `01`, then press "write".  LED3 should light up on the board.  Now try sending `00`, the led should turn off again.

What you are seeing is the GATT protocol in action.  GATT is the protocol all Bluetooth LE devices are supposed to use to communicate with each other.  The Bluetooth SIG wanted to make different Bluetooth devices as interchangeable as possible, so they defined a higher level protocol of attributes which can be read, written to, and subscribed to for changes.  The protocols in Bluetooth LE are designed to be asymettric, so that one device (i.e. a smartphone) does most of the work and the other device (i.e. a temperature sensor) can sleep most of the time to save power.  In the case of the Link layer, the low-power device does the advertising and the smartphone listens.  Moving up a level to GATT, the low-power device should be the GATT server, while the smartphone is the GATT client.  It's a bit strange on first glance since usually servers have much more compute resources than our phones or computers, but it makes sense when you realize that the IoT device is what has the information we need to query or mutate.  

#### Troubleshooting the build/flash process

Here are some things to check:

* Is your device adequately powered?  I have had this cause intermittent issues on numerous occasions.  One symptom of this is if the USB port suddenly dies.  USB ports have something called a polyfuse which will temporarily disconnect power if too much current is drawn.  Give it a few minutes and it will reset.  Sometimes the problem is more subtle, with the programming process simply failing for one reason or another.  The error messages will usually be unclear, saying things like "The operation attempted is unavailable due to readback protection" or "nrfjprog failed for an unknown reason".  Seeing a different error message on consecutive builds should be a big tip-off.  Consider powering your device through a high-quality externally-powered USB hub if you suspect that this might be your problem.

* Is the DK configured properly?  There is a switch (SW6) on the board which will disconnect the programmer and peripheral LEDs but leave the nRF52840 chip running for the purpose of current measurement.  It should be in the "DEFAULT" position to allow programming to take place.  There are traces which can be cut to perform current measurement, and once cut they will need to be jumpered or briged again during programming.  Finally there is a switch labeled "nRF power source" which should be in the VDD position while programming (other options might work if the proper power sources have been provided, I have not tested much).  The error messages I've seen associated with this problem are "There is no debugger connected to the PC." and "JLinkARM DLL reported an error.".

* Is USB passthrough on your virtual machine working correctly?  You could try running `lsusb` to ensure that a device named Segger is available to confirm.  I haven't encountered this problem myself so I can't provide many debugging suggestions.

### Setting up VSCode Remote Editing

The remote development extensions for VSCode allow browsing and editing code located on a remote machine with intellisense and full access to that machine's toolchain.

#### Creating an SSH alias for your host
Before you can connect from inside VSCode, you need to define an SSH Alias.  [SSH Aliases](https://www.howtogeek.com/75007/stupid-geek-tricks-use-your-ssh-config-file-to-create-aliases-for-hosts/) are short names for hosts which load up pre-defined configuration like which private key to use, and the username on the remote machine.  They are defined inside `~/.ssh/config`  Here is an example:

```
Host nrf_vagrant
  HostName 192.168.33.10
  User vagrant
  IdentityFile <your-path-to-dev-env>/nrf_dev_env/.vagrant/machines/default/virtualbox/private_key
```

You will want to customise this to the details of your machine, but that is the gist of it.  See the linked article for more information on the syntax of SSH Aliases.  Please test it by running `ssh nrf_vagrant` or whatever name you chose before continuing.

#### Setting up VSCode

Open the VSCode extensions pane and search for "Remote Development". Install the extension pack.  Note that this extension is unfortunately not available for VSCode variants like VSCodium.

Now you should see a new icon containing opposing angle brackets in the bottom left of your window.  Click it and select "Remote-SSH: Connect to Host...".  Then choose the host you just defined.  A new window will open.  Open the file explorer pane, and select "Open Folder".  Then select "nrf_sdk".  You should now be able to browse and edit code on the remote machine.  Try navigating to the blinky example and changing the delay between switching LEDs, then run `make flash` through the built-in terminal.

### Creating a project of your own

TODO: finish this section